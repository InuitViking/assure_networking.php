<?php

/**
* What this script does is to make sure that the server always has an internet connection.
* 1.	It checks if there's a backup of /etc/network/interfaces and /etc/resolv.conf
* 2.	It checks if there's an internet connection (both via IP and domain name)
* 3.	If there's no internet connection, it will take a backup of the current /etc/network/interfaces and /etc/resolv.conf
* 		and then replace the originals with the first backed up /etc/network/interfaces and /etc/resolv.conf.
* This script is a way of "going back to defaults" automatically (with CRON), in case people (like me) have played around
* with the networking configuration; just to be able to fall back in case you've messed that part upp ;)
*
* @AUTHOR			= Angutivik Casper Rúnur Tausen Hansen
*/

# Create options so people can defined their own paths
$longopts = array('backup-path:');
$shortopts = "b:";
$options = getopt($shortopts, $longopts);

$backupPath = ""; // This is just so that we avoid notices.

if(empty($options)){
	echo "Backup path not specified\n";
	exit;
}else{
	if(isset($options['b'])){
		$backupPath = $options['b'];
	}elseif(isset($options['backup-path'])){
		$backupPath = $options['backup-path'];
	}
}

## 1. CHECK IF FILES AND SUCH EXIST
$interfacesBackup	= $backupPath."/interfaces";
$resolvBackup		= $backupPath."/resolv.conf";

# Check if file/path exists and can be created
if(!file_exists($backupPath) && !is_dir($backupPath)){
	# Create direcetory
	mkdir($backupPath);
	echo "Backup directory didn't exist, so I created it.\n";
}

# Copy /etc/network/interfaces to backup directory if it doesn't already exist or is empty
clearstatcache();
if(!file_exists($interfacesBackup) || filesize($interfacesBackup) == 0){
	copy("/etc/network/interfaces", $interfacesBackup);
	echo "$interfacesBackup didn't exist, so I created it.\n";
}

# Copy /etc/resolv.conf to backup directory if it doesn't already exist or is empty
clearstatcache();
if(!file_exists($resolvBackup) || filesize($resolvBackup) == 0){
	copy("/etc/resolv.conf", $resolvBackup);
	echo "$resolvBackup didn't exist, so I created it.\n";
}


## 2. CHECK IF THERE'S AN INTERNET CONNECTION

# Function taken from here: https://stackoverflow.com/a/4860432/3613647
function is_connected(){
	$connectedHTTP = @fsockopen("www.example.com", 80);
	$connectedHTTPS = @fsockopen("www.example.com", 443);
	if ($connectedHTTP || $connectedHTTPS){
		$is_conn = true; //action when connected
		fclose($connectedHTTP);
		fclose($connectedHTTPS);
	}else{
		$is_conn = false; //action in connection failure
	}
	return $is_conn;
}

function attempt_fix_connection($backupPath){
	$backupCurrentPath = "$backupPath/current";
	if(!file_exists($backupCurrentPath) && !is_dir($backupCurrentPath)){
		mkdir($backupCurrentPath);
	}
	# Backing up edited files
	copy("/etc/network/interfaces", "$backupCurrentPath/interfaces");
	copy("/etc/resolv.conf", "$backupCurrentPath/resolv.conf");

	# Copying first backups to replace edited files
	copy("$backupPath/interfaces", "/etc/network/interfaces");
	copy("$backupPath/resolv.conf", "/etc/resolv.conf");

	# Restarting networking
	shell_exec("ifdown -a && sleep 10 && ifup --force -a");
	shell_exec("systemctl restart networking");

	# Checking internet connection
	if(is_connected()){
		echo "REGAINED CONNECTION TO THE INTERNET\n";
	}else{
		echo "FIX DID NOT WORK. QUITTING.\n";
		exit;
	}
}

if(is_connected()){
	echo "We got an internet connection! :D\n";
}else{
	echo "NO INTERNET CONNECTION; ATTEMPTING FIX.\n";
	attempt_fix_connection($backupPath);
}

?>

